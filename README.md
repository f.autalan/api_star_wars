# api_star_wars


### Installation of dependencies

```sh
npm install
```

### Environments

settings
```sh
PORT=8080
VERSION=v1
```

integration api star wars
```sh
SWAPI_BASE_URL=https://swapi.dev/api/
SWAPI_TIMEOUT=10000
SWAPI_SUJECT_VEHICLE=vehicles
SWAPI_SUJECT_PEOPLE=people
SWAPI_SUJECT_PLANETS=planets
```

Cache axios
```sh
CACHE_IGNORE=false
CACHE_TIME=120000
CACHE_EXCLUDE_QUERY=false
```