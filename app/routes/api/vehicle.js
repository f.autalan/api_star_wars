const Router = require('express').Router;
const Response = require('../../util/reponse');
const controller = require('../../controllers/vehicle-controller');

const router = Router();

router.get('/', async (req, res, next) => {
  try {
    const response = await controller.topExpensiveVehicles(req.query.amount);
    return Response.setResponseRaw(res, 200, response);
  } catch (error) {
    return next(error);
  }
});

router.get('/:lastName', async (req, res, next) => {
  try {
    const response = await controller.getAllVehicles(req.params.lastName);
    return Response.setResponseRaw(res, 200, response);
  } catch (error) {
    return next(error);
  }
});

module.exports = router;
