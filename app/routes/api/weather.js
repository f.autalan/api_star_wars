const Router = require('express').Router;
const Response = require('../../util/reponse');
const controller = require('../../controllers/weather-controller');

const router = Router();
router.get('/', async (req, res, next) => {
  try {
    const response = await controller.averagePopulation();
    return Response.setResponseRaw(res, 200, response);
  } catch (error) {
    return next(error);
  }
});

module.exports = router;
