const Router = require('express').Router;
const apiVehicle = require('./api/vehicle');
const apiWeather = require('./api/weather');

const router = Router();

router.use('/weather', apiWeather);
router.use('/vehicle', apiVehicle);

module.exports = router;
