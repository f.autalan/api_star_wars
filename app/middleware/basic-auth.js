const auth = require('basic-auth');
const config = require('../config/index').basicAuth;
const response = require('../util/reponse');

module.exports.basicAuth = (req, res, next) => {
  const user = auth(req);
  if (user && user.name === config.username && user.pass === config.password) {
    return next();
  }
  return response.setResponseWithError(res, {
    status: 401,
    message: 'Access denied',
    code: 'Unauthorized'
  });
};
