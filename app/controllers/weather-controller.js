const { toFinite, split, forEach, map, flattenDeep } = require('lodash');
const Service = require('../services/star-wars-service');
const { addWeather, updateWeathers, buildPages } = require('../util/map');
const config = require('../config/index').service.starWars;

const set = async (data, population, weathers) => {
  forEach(weathers, weather => {
    const clearWeather = weather.trim().toLowerCase();
    const objWeather = data[clearWeather];
    objWeather ? updateWeathers(objWeather, population) : addWeather(data, clearWeather, population);
  });
  return data;
};

const buildPopulation = async (plantes, population) => {
  forEach(plantes, async value => {
    const amountPopulation = toFinite(value.population);
    if (amountPopulation) {
      const weathers = split(value.climate, ',');
      set(population, amountPopulation, weathers);
    }
  });
  return population;
};

exports.averagePopulation = async () => {
  const response = await Service.all(config.sujectPlanet, 1);
  const cantPage = buildPages(response.count, config.quantityPerPage);
  const population = {};
  let plantes = new Array();
  await Promise.all(
    map(cantPage, async page => {
      const planet = await Service.all(config.sujectPlanet, page);
      plantes = plantes.concat(planet.results);
    })
  );

  return buildPopulation(plantes, population);
};
