const { toFinite, forEach, map } = require('lodash');

const Heap = require('collections/heap');
const Service = require('../services/star-wars-service');
const error = require('../error/error');
const { buildVehicle, buildPages } = require('../util/map');
const config = require('../config/index').service.starWars;

const allVehicles = async vehicles => {
  const response = await Service.all(config.sujectVehicle, 1);
  const cantPage = buildPages(response.count, config.quantityPerPage);
  await Promise.all(
    map(cantPage, async page => {
      const res = await Service.all(config.sujectVehicle, page);
      forEach(res.results, async value => {
        if (toFinite(value.cost_in_credits)) {
          vehicles.push(buildVehicle(value));
        }
      });
    })
  );
};

const topExpensiveVehicles = async (amount = 10) => {
  const heap = new Heap([], null, function(a, b) {
    return a.cost - b.cost;
  });
  await allVehicles(heap);
  if (amount > heap.length) {
    throw new error.CustomErrorHTTP('there is no such amount', 404, 'Error');
  }
  const top = [];
  for (let i = 0; i < amount; i++) {
    top.push(heap.pop());
  }
  return top;
};

const getAllVehicles = async lastName => {
  const peoples = await Service.search(config.sujectPeople, lastName);
  const urlVehicles = new Set([], (a, b) => {
    return a === b;
  });
  forEach(peoples.results, people => {
    urlVehicles.addEach(people.vehicles);
  });
  const response = await Promise.all(
    urlVehicles.map(async url => {
      const res = await Service.get(url);
      return buildVehicle(res);
    })
  );
  return response;
};

module.exports = {
  topExpensiveVehicles,
  getAllVehicles
};
