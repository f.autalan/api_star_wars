const requestService = require('./axios-service');
const starWars = require('../config/index').service.starWars;

const all = async (suject, page) => {
  const option = {
    url: `${starWars.url}/${suject}/?page=${page}`,
    method: 'GET',
    timeout: starWars.timeout
  };
  const response = await requestService.def(option);
  return response;
};

const search = async (suject, criterion) => {
  const option = {
    url: `${starWars.url}/${suject}?search=${criterion}`,
    method: 'GET',
    timeout: starWars.timeout
  };
  const response = await requestService.def(option);
  return response;
};

const get = async url => {
  const option = {
    url,
    method: 'GET',
    timeout: starWars.timeout
  };
  const response = await requestService.def(option);
  return response;
};

module.exports = {
  all,
  search,
  get
};
