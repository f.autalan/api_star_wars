const axios = require('axios');
const { setupCache } = require('axios-cache-adapter');
const config = require('../config/index').cache;

const cache = setupCache({
  maxAge: Number(config.time),
  exclude: { query: config.query },
  ignoreCache: config.active
});

const buildResponse = response => (response.data ? response.data : response);

const def = async options => {
  const instance = axios.create({
    adapter: cache.adapter,
    ...options
  });
  const response = await instance(options);
  return buildResponse(response);
};

module.exports = {
  def
};
