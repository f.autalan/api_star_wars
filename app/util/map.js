const { toNumber } = require('lodash');

const buildPages = (amount, pages) => {
  if (String(amount).length === 1) {
    return [1];
  }
  let x = Math.trunc(amount / pages);
  amount % pages !== 0 ? x++ : x;
  const result = [];
  for (let i = 1; i < x + 1; i++) {
    result.push(i);
  }
  return result;
};

const buildVehicle = data => {
  return {
    name: data.name,
    model: data.model,
    cost: toNumber(data.cost_in_credits),
    url: data.url
  };
};

const addWeather = (data, weather, population) => {
  data[weather] = {
    population,
    amount: 1,
    average: population
  };
  return data;
};

const updateWeathers = (data, population) => {
  data.population += population;
  data.amount += 1;
  data.average = data.population / data.amount;
  return data;
};

module.exports = {
  buildVehicle,
  addWeather,
  updateWeathers,
  buildPages
};
