const setResponseWithError = (res, { status, message, code = 'error' }) => {
  return res.status(status).send({ code, message });
};

const setResponseWithOk = (res, status, message, code = 'ok') => {
  return res.status(status).send({ code, message });
};

const setResponseRaw = (res, status, message) => {
  return res.status(status).send(message);
};

module.exports.setResponseWithError = setResponseWithError;
module.exports.setResponseWithOk = setResponseWithOk;
module.exports.setResponseRaw = setResponseRaw;
