require('dotenv').config();

module.exports = {
  service: {
    starWars: {
      url: process.env.SWAPI_BASE_URL,
      timeout: process.env.SWAPI_TIMEOUT,
      sujectVehicle: process.env.SWAPI_SUJECT_VEHICLE,
      sujectPeople: process.env.SWAPI_SUJECT_PEOPLE,
      sujectPlanet: process.env.SWAPI_SUJECT_PLANETS,
      quantityPerPage: +process.env.SWAPI_QUANTITY_PER_PAGE
    }
  },
  cache: {
    active: process.env.CACHE_IGNORE === 'true',
    time: process.env.CACHE_TIME,
    query: process.env.CACHE_EXCLUDE_QUERY === 'true'
  },
  basicAuth: {
    username: process.env.BASIC_AUTH_USERNAME,
    password: process.env.BASIC_AUTH_PASSWORD
  }
};
