/* eslint-disable no-prototype-builtins */
module.exports.paths = (basePath, routes, port) => {
  const Table = require('cli-table');
  const table = new Table({ head: ['Method', 'Path'] });
  console.log(`\nAPI listening on port: ${port}`);
  for (const key in routes) {
    if (routes.hasOwnProperty(key)) {
      let val = routes[key];
      if (val.route) {
        val = val.route;
        const _o = {};
        _o[val.stack[0].method.toUpperCase()] = [basePath + val.path];
        table.push(_o);
      }
    }
  }
  console.log(table.toString());
  return table;
};
