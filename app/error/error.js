const constant = require('../constant/index');

class CustomErrorHTTP extends Error {
  constructor(message, status, code) {
    super(message);
    this.name = constant.CUSTOM_ERROR;
    this.status = status;
    this.isCustomerError = true;
    this.code = code;
  }
}

module.exports = {
  CustomErrorHTTP
};
