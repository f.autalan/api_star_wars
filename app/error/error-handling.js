const constant = require('../constant/index');
const response = require('../util/reponse');

const map = {
  400: constant.BAD_REQUEST_MENSSAGE,
  404: constant.NOT_FOUNT_MENSSAGE,
  408: constant.REQUEST_TIMEOUT_ERROR_MENSSAGE,
  500: constant.INTERNAL_SERVER_ERORR_MENSSAGE
};

exports.handle = (error, req, res, next) => {
  let err = {
    status: 500,
    message: map[500],
    code: 'error'
  };
  // Error axios
  if (error.isAxiosError) {
    if (error.code === 'ECONNABORTED') {
      err = {
        status: 408,
        message: map[408],
        code: 'error'
      };
    } else {
      const status = map[error.response.status] ? error.response.status : 500;
      err = {
        status,
        message: map[status],
        code: 'error'
      };
    }
  }
  // Error customer
  if (error.isCustomerError) {
    err = {
      status: error.status,
      message: error.message,
      code: error.code
    };
  }
  return response.setResponseWithError(res, err);
};
