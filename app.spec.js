/* eslint-disable no-undef */
const request = require('supertest');
const { expect } = require('chai');
const app = require('./app');
const WeatherController = require('./app/controllers/weather-controller');
const vehicleController = require('./app/controllers/vehicle-controller');
const config = require('./app/config/index').basicAuth;

jest.mock('./app/controllers/weather-controller');
jest.mock('./app/controllers/vehicle-controller');

const baseUrl = 'api/star/wars';
const version = process.env.VERSION;

describe('app testing', () => {
  describe('401 Unauthorized', () => {
    it('weather', async () => {
      await request(app).get(`/${baseUrl}/${version}/weather`).expect('Content-Type', /json/).expect(401);
    });
    it('vehicle', async () => {
      await request(app)
        .get(`/${baseUrl}/${version}/vehicle?amount=3`)
        .expect('Content-Type', /json/)
        .expect(401);
    });
    it('vehicle for Skywalker', async () => {
      await request(app)
        .get(`/${baseUrl}/${version}/vehicle/Skywalker`)
        .expect('Content-Type', /json/)
        .expect(401);
    });
  });
  describe('route weather', () => {
    it('response whit json containin a weather susseful', async () => {
      const myMock = WeatherController.averagePopulation.mockResolvedValue({
        arid: {
          population: 107937200000,
          amount: 8,
          average: 13492150000,
        },
        temperate: {
          population: 1667285712500,
          amount: 29,
          average: 57492610775.86207,
        },
      });
      await request(app)
        .get(`/${baseUrl}/${version}/weather`)
        .auth(config.username, config.password)
        .expect('Content-Type', /json/)
        .expect(200)
        .then((response) => {
          expect(myMock.mock.calls.length).to.equal(1);
          const data = response.body;
          expect(data).to.be.instanceOf(Object);
          const subjet = data[Object.keys(data)[0]];
          expect(subjet).to.be.instanceOf(Object);
          expect(subjet).to.have.property('population');
          expect(subjet).to.have.property('amount');
          expect(subjet).to.have.property('average');
        });
    });
  });
  describe('route vehicle', () => {
    it('json response containing top 3 vehicle successful', async () => {
      const myMock = vehicleController.topExpensiveVehicles.mockResolvedValue([
        {
          name: 'Clone turbo tank',
          model: 'HAVw A6 Juggernaut',
          cost: 350000,
          url: 'https://swapi.dev/api/vehicles/71/',
        },
        {
          name: 'Sail barge',
          model: 'Modified Luxury Sail Barge',
          cost: 285000,
          url: 'https://swapi.dev/api/vehicles/24/',
        },
        {
          name: 'C-9979 landing craft',
          model: 'C-9979 landing craft',
          cost: 200000,
          url: 'https://swapi.dev/api/vehicles/37/',
        },
      ]);
      await request(app)
        .get(`/${baseUrl}/${version}/vehicle?amount=3`)
        .auth(config.username, config.password)
        .expect('Content-Type', /json/)
        .expect(200)
        .then((response) => {
          expect(myMock.mock.calls.length).to.equal(1);
          const data = response.body;
          expect(data).to.be.instanceOf(Array);
          expect(data.length).to.equal(3);
          const subjet = data[0];
          expect(subjet).to.be.instanceOf(Object);
          expect(subjet).to.have.property('name');
          expect(subjet).to.have.property('model');
          expect(subjet).to.have.property('cost');
          expect(subjet).to.have.property('url');
        });
    });
  });
  describe('route vehicle for driver', () => {
    it('json response containing forecast successful', async () => {
      const myMock = vehicleController.getAllVehicles.mockResolvedValue([
        {
          name: 'Snowspeeder',
          model: 't-47 airspeeder',
          cost: 'unknown',
          url: 'https://swapi.dev/api/vehicles/14/',
        },
        {
          name: 'Imperial Speeder Bike',
          model: '74-Z speeder bike',
          cost: 8000,
          url: 'https://swapi.dev/api/vehicles/30/',
        },
        {
          name: 'Zephyr-G swoop bike',
          model: 'Zephyr-G swoop bike',
          cost: 5750,
          url: 'https://swapi.dev/api/vehicles/44/',
        },
      ]);
      await request(app)
        .get(`/${baseUrl}/${version}/vehicle/Skywalker`)
        .auth(config.username, config.password)
        .expect('Content-Type', /json/)
        .expect(200)
        .then((response) => {
          expect(myMock.mock.calls.length).to.equal(1);
          const data = response.body;
          expect(data).to.be.instanceOf(Array);
          const subjet = data[0];
          expect(subjet).to.be.instanceOf(Object);
          expect(subjet).to.have.property('name');
          expect(subjet).to.have.property('model');
          expect(subjet).to.have.property('cost');
          expect(subjet).to.have.property('url');
        });
    });
  });
});
