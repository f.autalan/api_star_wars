const express = require('express');
const bodyParser = require('body-parser');
const routes = require('./app/routes/routes');
const errors = require('./app/error/error-handling');
const { basicAuth } = require('./app/middleware/basic-auth');
require('dotenv').config();

const port = process.env.PORT || 8080;

const baseUrl = 'api/star/wars';
const version = process.env.VERSION;

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.get(`/${baseUrl}/health`, (req, res) => {
  console.log('caca');
  res.status(200).send({ code: 'OK', message: 'up and running' });
});

app.use(basicAuth);
app.use(`/${baseUrl}/${version}`, routes);

// Error route
app.use('/*', (req, res) =>
  res.status(404).send({
    message: 'Route not found'
  })
);

// Error Handler
app.use(errors.handle);

app.listen(port, () => {
  // eslint-disable-next-line no-console
  console.log(`\nAPI listening on port: ${port}`);
});

module.exports = app;
